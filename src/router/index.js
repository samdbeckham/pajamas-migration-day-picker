import { createRouter, createWebHistory } from "vue-router";
import { ROUTES } from "../constants";
import HomeView from "../views/HomeView.vue";
import QuestionView from "../views/QuestionView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: ROUTES.HOME,
      name: "home",
      component: HomeView,
    },
    {
      path: ROUTES.QUESTIONS,
      name: "questions",
      component: QuestionView,
    },
  ],
});

export default router;
